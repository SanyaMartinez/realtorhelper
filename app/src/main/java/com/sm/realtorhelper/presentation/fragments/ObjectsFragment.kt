package com.sm.realtorhelper.presentation.fragments

import android.os.Bundle
import android.view.View
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import androidx.navigation.fragment.findNavController
import by.kirich1409.viewbindingdelegate.viewBinding
import com.sm.realtorhelper.R
import com.sm.realtorhelper.databinding.FragmentObjectsBinding
import com.sm.realtorhelper.di.utils.viewmodels.ViewModelFactory
import com.sm.realtorhelper.domain.models.Object
import com.sm.realtorhelper.presentation.adapters.ObjectsAdapter
import com.sm.realtorhelper.presentation.viewmodels.ObjectsViewModel
import javax.inject.Inject

class ObjectsFragment @Inject constructor(
    private val factory: ViewModelFactory,
) : Fragment(R.layout.fragment_objects) {

    private val viewModel: ObjectsViewModel by viewModels { factory }

    private val binding: FragmentObjectsBinding by viewBinding()

    private val objectsClickListener = { item: Object ->
        // TODO: some action
    }
    private val objectsAdapter = ObjectsAdapter(objectsClickListener)

    private val objectsObserver = Observer<List<Object>?> { list ->
        list?.let {
            objectsAdapter.replaceData(it)
        }
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        viewModel.objects.observe(viewLifecycleOwner, objectsObserver)

        binding.apply {
            objectsList.adapter = objectsAdapter
            objectsButtonAdd.setOnClickListener {
                val navDirections = ObjectsFragmentDirections.toAddObject()
                findNavController().navigate(navDirections)
            }
        }
    }
}
