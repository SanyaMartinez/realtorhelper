package com.sm.realtorhelper.presentation.fragments

import android.os.Bundle
import android.view.View
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import androidx.navigation.fragment.findNavController
import by.kirich1409.viewbindingdelegate.viewBinding
import com.sm.realtorhelper.R
import com.sm.realtorhelper.databinding.FragmentContactsBinding
import com.sm.realtorhelper.di.utils.viewmodels.ViewModelFactory
import com.sm.realtorhelper.domain.models.Contact
import com.sm.realtorhelper.presentation.adapters.ContactsAdapter
import com.sm.realtorhelper.presentation.viewmodels.ContactsViewModel
import javax.inject.Inject

class ContactsFragment @Inject constructor(
    private val factory: ViewModelFactory,
) : Fragment(R.layout.fragment_contacts) {

    private val viewModel: ContactsViewModel by viewModels { factory }

    private val binding: FragmentContactsBinding by viewBinding()

    private val contactsClickListener = { item: Contact ->
        // TODO: call to item.phone
    }
    private val contactsAdapter = ContactsAdapter(contactsClickListener)

    private val contactsObserver = Observer<List<Contact>?> { list ->
        list?.let {
            contactsAdapter.replaceData(it)
        }
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        viewModel.contacts.observe(viewLifecycleOwner, contactsObserver)

        binding.apply {
            contactsList.adapter = contactsAdapter
            contactsButtonAdd.setOnClickListener {
                val navDirections = ContactsFragmentDirections.toAddContact()
                findNavController().navigate(navDirections)
            }
        }
    }
}
