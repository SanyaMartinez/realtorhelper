package com.sm.realtorhelper.presentation.fragments

import android.os.Bundle
import android.view.View
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import by.kirich1409.viewbindingdelegate.viewBinding
import com.sm.realtorhelper.R
import com.sm.realtorhelper.databinding.FragmentEventsBinding
import com.sm.realtorhelper.presentation.viewmodels.EventsViewModel

class EventsFragment : Fragment(R.layout.fragment_events) {

    private lateinit var eventsViewModel: EventsViewModel

    private val binding: FragmentEventsBinding by viewBinding()

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        eventsViewModel = ViewModelProvider(this).get(EventsViewModel::class.java)
    }
}
