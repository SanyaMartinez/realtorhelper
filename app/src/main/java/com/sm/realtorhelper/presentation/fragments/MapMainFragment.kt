package com.sm.realtorhelper.presentation.fragments

import android.os.Bundle
import android.view.View
import androidx.fragment.app.Fragment
import by.kirich1409.viewbindingdelegate.viewBinding
import com.sm.realtorhelper.R
import com.sm.realtorhelper.databinding.FragmentMapMainBinding

class MapMainFragment : Fragment(R.layout.fragment_map_main) {

    private val binding: FragmentMapMainBinding by viewBinding()

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding.webview.apply {
            settings.javaScriptEnabled = true
            loadUrl(GOOGLE_MAPS_URL)
        }
    }

    companion object {
        private const val GOOGLE_MAPS_URL = "https://www.google.ru/maps/@55.1596248,61.3693623,14.75z"
    }
}