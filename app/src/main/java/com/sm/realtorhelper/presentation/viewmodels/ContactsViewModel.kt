package com.sm.realtorhelper.presentation.viewmodels

import androidx.lifecycle.LiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.asLiveData
import com.sm.realtorhelper.domain.models.Contact
import com.sm.realtorhelper.domain.usecases.contacts.getallcontacts.GetAllContactsUseCase
import javax.inject.Inject

class ContactsViewModel @Inject constructor(
    private val getAllContactsUseCase: GetAllContactsUseCase,
) : ViewModel() {

    val contacts: LiveData<List<Contact>?>
        get() = getAllContactsUseCase().asLiveData()
}
