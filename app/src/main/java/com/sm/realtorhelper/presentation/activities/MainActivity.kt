package com.sm.realtorhelper.presentation.activities

import android.os.Bundle
import androidx.appcompat.widget.PopupMenu
import androidx.navigation.NavController
import androidx.navigation.findNavController
import com.google.android.material.bottomnavigation.BottomNavigationView
import com.sm.realtorhelper.R
import com.sm.realtorhelper.databinding.ActivityMainBinding
import com.sm.realtorhelper.di.utils.fragments.FragmentFactory
import dagger.android.support.DaggerAppCompatActivity
import javax.inject.Inject

class MainActivity : DaggerAppCompatActivity() {

    private lateinit var binding: ActivityMainBinding
    private lateinit var navController: NavController

    private val bottomNavView: BottomNavigationView
        get() = binding.bottomNavigation

    private val bottomNavClickListener = BottomNavigationView.OnNavigationItemSelectedListener {
        when (it.itemId) {
            R.id.menuMap -> toMap()
            R.id.menuHome -> toHome()
            R.id.menuAdd -> showAddMenu()
        }
        true
    }

    @Inject
    fun initFragmentFactory(fragmentFactory: FragmentFactory) {
        supportFragmentManager.fragmentFactory = fragmentFactory
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)

        bottomNavView.selectedItemId = R.id.menuHome

        navController = findNavController(R.id.navHostFragment)
        navController.addOnDestinationChangedListener { _, destination, _ ->
            title = destination.label
        }

        bottomNavView.setOnNavigationItemSelectedListener(bottomNavClickListener)
    }

    private fun toMap() = navController.navigate(R.id.toMap)

    private fun toHome() = navController.navigate(R.id.toHome)

    private fun showAddMenu() {
        val popup = PopupMenu(this, findViewById(R.id.menuAdd))
        popup.menuInflater.inflate(R.menu.bottom_nav_sub_menu, popup.menu)
        popup.setOnMenuItemClickListener {
            when (it.itemId) {
                R.id.addContact -> toAddContract()
                R.id.addObject -> toAddObject()
                R.id.addEvent -> toAddEvent()
            }
            true
        }
        popup.show()
    }

    private fun toAddContract() = navController.navigate(R.id.toAddContact)
    private fun toAddObject() = navController.navigate(R.id.toAddObject)
    private fun toAddEvent() = navController.navigate(R.id.toAddEvent)
}