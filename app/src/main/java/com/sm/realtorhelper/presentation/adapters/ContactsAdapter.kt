package com.sm.realtorhelper.presentation.adapters

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.sm.realtorhelper.R
import com.sm.realtorhelper.databinding.ContactsListItemBinding
import com.sm.realtorhelper.domain.models.Contact

class ContactsAdapter(
    private val itemClickListener: ItemClickListener
): RecyclerView.Adapter<ContactsAdapter.ViewHolder>() {

    private val items: MutableList<Contact> = mutableListOf()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val binding = ContactsListItemBinding.inflate(
            LayoutInflater.from(parent.context), parent, false
        )
        return ViewHolder(binding)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        items[position].let { item ->
            holder.bind(item, itemClickListener)
        }
    }

    override fun getItemCount() = items.size

    fun replaceData(list: List<Contact>) {
        items.apply {
            clear()
            addAll(list)
        }
        notifyDataSetChanged()
    }

    class ViewHolder(private val binding: ContactsListItemBinding): RecyclerView.ViewHolder(binding.root) {
        fun bind(item: Contact, listener: ItemClickListener) {
            binding.apply {
                contactId.text = root.context.getString(R.string.contacts_item_id_text, item.id)
                contactName.text = root.context.getString(R.string.contacts_item_name_text, item.firstName, item.secondName)
                contactPhone.text = item.phone

                contactItem.setOnClickListener {
                    listener.onClick(item)
                }
            }
        }
    }

    fun interface ItemClickListener {
        fun onClick(item: Contact)
    }
}
