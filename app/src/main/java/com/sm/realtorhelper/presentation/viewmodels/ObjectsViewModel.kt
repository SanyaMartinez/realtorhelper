package com.sm.realtorhelper.presentation.viewmodels

import androidx.lifecycle.LiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.asLiveData
import com.sm.realtorhelper.domain.models.Object
import com.sm.realtorhelper.domain.usecases.objects.getall.GetAllObjectsUseCase
import javax.inject.Inject

class ObjectsViewModel @Inject constructor(
    private val getAllObjectsUseCase: GetAllObjectsUseCase,
) : ViewModel() {

    val objects: LiveData<List<Object>?>
        get() = getAllObjectsUseCase().asLiveData()
}
