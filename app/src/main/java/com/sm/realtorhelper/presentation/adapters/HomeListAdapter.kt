package com.sm.realtorhelper.presentation.adapters

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import com.sm.realtorhelper.databinding.HomeListItemBinding
import com.sm.realtorhelper.presentation.viewmodels.HomeListUiModel

class HomeListAdapter(
    private val itemClickListener: ItemClickListener
): RecyclerView.Adapter<HomeListAdapter.ViewHolder>() {

    private val items: MutableList<HomeListUiModel> = mutableListOf()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val binding = HomeListItemBinding.inflate(
            LayoutInflater.from(parent.context), parent, false
        )
        return ViewHolder(binding)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        items[position].let { item ->
            holder.bind(item, itemClickListener)
        }
    }

    override fun getItemCount() = items.size

    fun replaceData(list: List<HomeListUiModel>) {
        items.apply {
            clear()
            addAll(list)
        }
        notifyDataSetChanged()
    }

    class ViewHolder(private val binding: HomeListItemBinding): RecyclerView.ViewHolder(binding.root) {
        fun bind(item: HomeListUiModel, listener: ItemClickListener) {
            binding.apply {
                homeItemIcon.setImageDrawable(ContextCompat.getDrawable(root.context, item.iconId))
                homeItemTitle.setText(item.titleId)
                homeItemCount.text = item.count.toString()

                homeItem.setOnClickListener {
                    listener.onClick(item)
                }
            }
        }
    }

    fun interface ItemClickListener {
        fun onClick(item: HomeListUiModel)
    }
}
