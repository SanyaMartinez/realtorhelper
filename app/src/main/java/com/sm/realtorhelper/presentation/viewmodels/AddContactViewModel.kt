package com.sm.realtorhelper.presentation.viewmodels

import androidx.lifecycle.ViewModel
import androidx.lifecycle.asLiveData
import androidx.lifecycle.viewModelScope
import com.sm.realtorhelper.data.models.ContactInputInfo
import com.sm.realtorhelper.domain.models.Contact
import com.sm.realtorhelper.domain.usecases.contacts.addcontact.AddContactUseCase
import com.sm.realtorhelper.domain.usecases.contacts.validatecontact.ValidateContactUseCase
import kotlinx.coroutines.Dispatchers.IO
import kotlinx.coroutines.launch
import javax.inject.Inject

class AddContactViewModel @Inject constructor(
    private val addContactUseCase: AddContactUseCase,
    private val validateContactUseCase: ValidateContactUseCase,
) : ViewModel() {

    val validationResult = validateContactUseCase.validationResult.asLiveData()

    fun validateData(
        firstName: String?,
        secondName: String?,
        phone: String?,
        address: String?,
        email: String?,
    ) {
        val inputInfo = ContactInputInfo(firstName, secondName, phone, address, email)
        viewModelScope.launch(IO) {
            validateContactUseCase.validate(inputInfo)
        }
    }

    fun addContact(contact: Contact) {
        viewModelScope.launch(IO) {
            addContactUseCase(contact)
        }
    }
}