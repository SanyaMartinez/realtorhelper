package com.sm.realtorhelper.presentation.fragments

import android.os.Bundle
import android.view.View
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import by.kirich1409.viewbindingdelegate.viewBinding
import com.sm.realtorhelper.R
import com.sm.realtorhelper.databinding.FragmentAddObjectBinding
import com.sm.realtorhelper.presentation.viewmodels.AddObjectViewModel

class AddObjectFragment : Fragment(R.layout.fragment_add_object) {

    private lateinit var addObjectViewModel: AddObjectViewModel

    private val binding: FragmentAddObjectBinding by viewBinding()

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        addObjectViewModel = ViewModelProvider(this).get(AddObjectViewModel::class.java)
    }
}
