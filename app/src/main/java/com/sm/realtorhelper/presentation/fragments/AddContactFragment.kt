package com.sm.realtorhelper.presentation.fragments

import android.os.Bundle
import android.view.View
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import androidx.navigation.fragment.findNavController
import by.kirich1409.viewbindingdelegate.viewBinding
import com.sm.realtorhelper.R
import com.sm.realtorhelper.databinding.FragmentAddContactBinding
import com.sm.realtorhelper.di.utils.viewmodels.ViewModelFactory
import com.sm.realtorhelper.domain.models.Contact
import com.sm.realtorhelper.domain.usecases.contacts.validatecontact.ContactField
import com.sm.realtorhelper.domain.usecases.contacts.validatecontact.ContactValidation
import com.sm.realtorhelper.domain.usecases.contacts.validatecontact.ErrorType
import com.sm.realtorhelper.presentation.viewmodels.AddContactViewModel
import java.util.*
import javax.inject.Inject

class AddContactFragment @Inject constructor(
    private val factory: ViewModelFactory,
) : Fragment(R.layout.fragment_add_contact) {

    private val viewModel: AddContactViewModel by viewModels { factory }

    private val binding: FragmentAddContactBinding by viewBinding()

    private val validationResultObserver = Observer<ContactValidation?> { result ->
        when (result) {
            is ContactValidation.Success -> addContact(result.contact)
            is ContactValidation.Failure -> showErrors(result.errors)
        }
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        viewModel.validationResult.observe(viewLifecycleOwner, validationResultObserver)

        binding.addContactButtonSave.setOnClickListener { validateData() }
    }

    private fun validateData() {
        binding.run {
            val firstName = firstNameEditText.text?.toString()
            val secondName = secondNameEditText.text?.toString()
            val phone = phoneEditText.text?.toString()
            val address = addressEditText.text?.toString()
            val email = emailEditText.text?.toString()

            viewModel.validateData(firstName, secondName, phone, address, email)
        }
    }

    private fun addContact(contact: Contact) {
        viewModel.addContact(contact)
        findNavController().navigateUp()
    }

    private fun showErrors(errors: EnumMap<ContactField, ErrorType?>) {
        errors.forEach { pair ->
            val (field, errorType) = pair
            val errorMessage = when (errorType) {
                ErrorType.EMPTY -> getString(R.string.add_contact_error_empty)
                ErrorType.NOT_CORRECT -> getString(R.string.add_contact_error_not_correct)
                else -> ""
            }

            when (field) {
                ContactField.FIRST_NAME -> binding.firstNameInputLayout.error = errorMessage
                ContactField.SECOND_NAME -> binding.secondNameInputLayout.error = errorMessage
                ContactField.PHONE -> binding.phoneInputLayout.error = errorMessage
                ContactField.ADDRESS -> binding.addressInputLayout.error = errorMessage
                ContactField.EMAIL -> binding.emailInputLayout.error = errorMessage
                else -> return@forEach
            }
        }
    }
}
