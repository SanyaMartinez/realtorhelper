package com.sm.realtorhelper.presentation.viewmodels

import androidx.lifecycle.LiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.asLiveData
import androidx.navigation.NavDirections
import com.sm.realtorhelper.R
import com.sm.realtorhelper.domain.usecases.contacts.getcontactscount.GetContactsCountUseCase
import com.sm.realtorhelper.domain.usecases.objects.getobjectscount.GetObjectsCountUseCase
import com.sm.realtorhelper.presentation.fragments.HomeFragmentDirections
import kotlinx.coroutines.flow.combineTransform
import javax.inject.Inject

class HomeViewModel @Inject constructor(
    getContactsCountUseCase: GetContactsCountUseCase,
    getObjectsCountUseCase: GetObjectsCountUseCase,
    // TODO events
) : ViewModel() {

    private val _listInfo = combineTransform(
        getContactsCountUseCase(),
        getObjectsCountUseCase(),
        getContactsCountUseCase(), // TODO
    ) { contactsCount, objectsCount, eventsCount ->
        val data = getData(contactsCount, objectsCount, 0)
        emit(data)
    }

    val listInfo: LiveData<List<HomeListUiModel>?>
        get() = _listInfo.asLiveData()

    // TODO
    private fun getData(
        contactsCount: Int,
        objectsCount: Int,
        eventsCount: Int,
    ): List<HomeListUiModel> {
        val hardcodeInfo = listOf(
            HomeListUiModel(
                iconId = R.drawable.ic_contacts,
                titleId = R.string.home_list_title_contacts,
                count = contactsCount,
                navDirections = HomeFragmentDirections.toContacts()
            ),
            HomeListUiModel(
                iconId = R.drawable.ic_objects,
                titleId = R.string.home_list_title_objects,
                count = objectsCount,
                navDirections = HomeFragmentDirections.toObjects()
            ),
            HomeListUiModel(
                iconId = R.drawable.ic_calendar,
                titleId = R.string.home_list_title_events,
                count = eventsCount,
                navDirections = HomeFragmentDirections.toEvents()
            ),
        )
        return hardcodeInfo
    }
}

//TODO
data class HomeListUiModel(
    val iconId: Int,
    val titleId: Int,
    val count: Int,
    val navDirections: NavDirections,
)