package com.sm.realtorhelper.presentation.fragments

import android.os.Bundle
import android.view.View
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import com.sm.realtorhelper.databinding.FragmentHomeBinding
import com.sm.realtorhelper.presentation.viewmodels.HomeViewModel
import by.kirich1409.viewbindingdelegate.viewBinding
import com.sm.realtorhelper.R
import com.sm.realtorhelper.di.utils.viewmodels.ViewModelFactory
import com.sm.realtorhelper.presentation.adapters.HomeListAdapter
import com.sm.realtorhelper.presentation.viewmodels.ContactsViewModel
import com.sm.realtorhelper.presentation.viewmodels.HomeListUiModel
import javax.inject.Inject

class HomeFragment @Inject constructor(
    private val factory: ViewModelFactory,
) : Fragment(R.layout.fragment_home) {

    private val viewModel: HomeViewModel by viewModels { factory }

    private val binding: FragmentHomeBinding by viewBinding()

    private val homeItemClickListener = { item: HomeListUiModel ->
        findNavController().navigate(item.navDirections)
    }
    private val homeListAdapter = HomeListAdapter(homeItemClickListener)

    private val homeListObserver = Observer<List<HomeListUiModel>?> { list ->
        list?.let {
            homeListAdapter.replaceData(it)
        }
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        viewModel.listInfo.observe(viewLifecycleOwner, homeListObserver)

        binding.homeList.adapter = homeListAdapter
    }
}
