package com.sm.realtorhelper.presentation.fragments

import android.os.Bundle
import android.view.View
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import by.kirich1409.viewbindingdelegate.viewBinding
import com.sm.realtorhelper.R
import com.sm.realtorhelper.databinding.FragmentAddEventBinding
import com.sm.realtorhelper.presentation.viewmodels.AddEventViewModel

class AddEventFragment : Fragment(R.layout.fragment_add_event) {

    private lateinit var addEventViewModel: AddEventViewModel

    private val binding: FragmentAddEventBinding by viewBinding()

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        addEventViewModel = ViewModelProvider(this).get(AddEventViewModel::class.java)
    }
}
