package com.sm.realtorhelper.presentation.adapters

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.sm.realtorhelper.R
import com.sm.realtorhelper.databinding.ObjectListItemBinding
import com.sm.realtorhelper.domain.models.Object

class ObjectsAdapter(
    private val itemClickListener: ItemClickListener
): RecyclerView.Adapter<ObjectsAdapter.ViewHolder>() {

    private val items: MutableList<Object> = mutableListOf()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val binding = ObjectListItemBinding.inflate(
            LayoutInflater.from(parent.context), parent, false
        )
        return ViewHolder(binding)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        items[position].let { item ->
            holder.bind(item, itemClickListener)
        }
    }

    override fun getItemCount() = items.size

    fun replaceData(list: List<Object>) {
        items.apply {
            clear()
            addAll(list)
        }
        notifyDataSetChanged()
    }

    class ViewHolder(private val binding: ObjectListItemBinding): RecyclerView.ViewHolder(binding.root) {
        fun bind(item: Object, listener: ItemClickListener) {
            binding.apply {
                objectId.text = root.context.getString(R.string.objects_item_id_text, item.id)
                objectPrice.text = root.context.getString(R.string.objects_item_price_text, item.price)
                objectDescription.text = root.context.getString(R.string.objects_item_description_text, item.description, item.square)
                objectAddress.text = item.address

                objectItem.setOnClickListener {
                    listener.onClick(item)
                }
            }
        }
    }

    fun interface ItemClickListener {
        fun onClick(item: Object)
    }
}
