package com.sm.realtorhelper.di.modules

import android.app.Application
import androidx.room.Room
import com.sm.realtorhelper.data.localsource.AppDatabase
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
class LocalSourceModule {
    @Provides
    @Singleton
    fun provideAppDatabase(application: Application) = Room
        .databaseBuilder(application, AppDatabase::class.java, AppDatabase.DB_NAME)
        .addMigrations(
            // future migrations
        )
        .build()

    @Provides
    @Singleton
    fun provideContactDao(appDatabase: AppDatabase) = appDatabase.contactDao()

    @Provides
    @Singleton
    fun provideObjectDao(appDatabase: AppDatabase) = appDatabase.objectDao()
}
