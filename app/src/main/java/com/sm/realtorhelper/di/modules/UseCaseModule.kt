package com.sm.realtorhelper.di.modules

import com.sm.realtorhelper.domain.usecases.contacts.addcontact.AddContactUseCase
import com.sm.realtorhelper.domain.usecases.contacts.addcontact.AddContactUseCaseImpl
import com.sm.realtorhelper.domain.usecases.contacts.getallcontacts.GetAllContactsUseCase
import com.sm.realtorhelper.domain.usecases.contacts.getallcontacts.GetAllContactsUseCaseImpl
import com.sm.realtorhelper.domain.usecases.contacts.getcontact.GetContactUseCase
import com.sm.realtorhelper.domain.usecases.contacts.getcontact.GetContactUseCaseImpl
import com.sm.realtorhelper.domain.usecases.contacts.getcontactscount.GetContactsCountUseCase
import com.sm.realtorhelper.domain.usecases.contacts.getcontactscount.GetContactsCountUseCaseImpl
import com.sm.realtorhelper.domain.usecases.contacts.validatecontact.ValidateContactUseCase
import com.sm.realtorhelper.domain.usecases.contacts.validatecontact.ValidateContactUseCaseImpl
import com.sm.realtorhelper.domain.usecases.objects.getall.GetAllObjectsUseCase
import com.sm.realtorhelper.domain.usecases.objects.getall.GetAllObjectsUseCaseImpl
import com.sm.realtorhelper.domain.usecases.objects.getobjectscount.GetObjectsCountUseCase
import com.sm.realtorhelper.domain.usecases.objects.getobjectscount.GetObjectsCountUseCaseImpl
import dagger.Binds
import dagger.Module

@Module
interface UseCaseModule {

    @Binds
    fun bindGetAllContactsUseCase(getAllContactsUseCaseImpl: GetAllContactsUseCaseImpl): GetAllContactsUseCase

    @Binds
    fun bindGetContactUseCase(getContactUseCaseImpl: GetContactUseCaseImpl): GetContactUseCase

    @Binds
    fun bindGetContactsCountUseCase(getContactsCountUseCaseImpl: GetContactsCountUseCaseImpl): GetContactsCountUseCase

    @Binds
    fun bindAddContactUseCase(addContactUseCaseImpl: AddContactUseCaseImpl): AddContactUseCase

    @Binds
    fun bindValidateContactUseCase(validateContactUseCaseImpl: ValidateContactUseCaseImpl): ValidateContactUseCase

    @Binds
    fun bindGetObjectsCountUseCase(getObjectsCountUseCaseImpl: GetObjectsCountUseCaseImpl): GetObjectsCountUseCase

    @Binds
    fun bindGetAllObjectsUseCase(getAllObjectsUseCaseImpl: GetAllObjectsUseCaseImpl): GetAllObjectsUseCase
}
