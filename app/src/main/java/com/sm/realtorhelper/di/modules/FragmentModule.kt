package com.sm.realtorhelper.di.modules

import androidx.fragment.app.Fragment
import com.sm.realtorhelper.di.utils.fragments.FragmentKey
import com.sm.realtorhelper.presentation.fragments.AddContactFragment
import com.sm.realtorhelper.presentation.fragments.ContactsFragment
import com.sm.realtorhelper.presentation.fragments.HomeFragment
import com.sm.realtorhelper.presentation.fragments.ObjectsFragment
import dagger.Binds
import dagger.Module
import dagger.multibindings.IntoMap

@Module
interface FragmentModule {

    @Binds
    @IntoMap
    @FragmentKey(ContactsFragment::class)
    abstract fun bindContactsFragment(fragment : ContactsFragment) : Fragment

    @Binds
    @IntoMap
    @FragmentKey(AddContactFragment::class)
    abstract fun bindAddContactFragment(fragment : AddContactFragment) : Fragment

    @Binds
    @IntoMap
    @FragmentKey(ObjectsFragment::class)
    abstract fun bindObjectsFragment(fragment : ObjectsFragment) : Fragment

    @Binds
    @IntoMap
    @FragmentKey(HomeFragment::class)
    abstract fun bindHomeFragment(fragment : HomeFragment) : Fragment
}
