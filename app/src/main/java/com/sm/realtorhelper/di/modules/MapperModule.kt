package com.sm.realtorhelper.di.modules

import com.sm.realtorhelper.data.mappers.ContactMapper
import com.sm.realtorhelper.data.mappers.ObjectMapper
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
class MapperModule {

    @Provides
    @Singleton
    fun provideContactMapper() = ContactMapper()

    @Provides
    @Singleton
    fun provideObjectMapper() = ObjectMapper()
}
