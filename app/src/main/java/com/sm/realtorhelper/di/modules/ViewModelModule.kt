package com.sm.realtorhelper.di.modules

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.sm.realtorhelper.di.utils.viewmodels.ViewModelFactory
import com.sm.realtorhelper.di.utils.viewmodels.ViewModelKey
import com.sm.realtorhelper.presentation.viewmodels.AddContactViewModel
import com.sm.realtorhelper.presentation.viewmodels.ContactsViewModel
import com.sm.realtorhelper.presentation.viewmodels.HomeViewModel
import com.sm.realtorhelper.presentation.viewmodels.ObjectsViewModel
import dagger.Binds
import dagger.Module
import dagger.multibindings.IntoMap

@Module
interface ViewModelModule {

    @Binds
    fun bindViewModelFactory(factory: ViewModelFactory): ViewModelProvider.Factory

    @Binds
    @IntoMap
    @ViewModelKey(ContactsViewModel::class)
    fun bindContactsViewModel(viewModel: ContactsViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(AddContactViewModel::class)
    fun bindAddContactViewModel(viewModel: AddContactViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(HomeViewModel::class)
    fun bindHomeViewModel(viewModel: HomeViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(ObjectsViewModel::class)
    fun bindObjectsViewModel(viewModel: ObjectsViewModel): ViewModel
}
