package com.sm.realtorhelper.di.modules

import com.sm.realtorhelper.data.repositories.ContactsRepositoryImpl
import com.sm.realtorhelper.data.repositories.ObjectsRepositoryImpl
import com.sm.realtorhelper.domain.repositories.ContactsRepository
import com.sm.realtorhelper.domain.repositories.ObjectsRepository
import dagger.Binds
import dagger.Module

@Module
interface RepositoryModule {

    @Binds
    fun bindContactsRepository(contactsRepositoryImpl: ContactsRepositoryImpl): ContactsRepository

    @Binds
    fun bindObjectsRepository(objectsRepositoryImpl: ObjectsRepositoryImpl): ObjectsRepository
}
