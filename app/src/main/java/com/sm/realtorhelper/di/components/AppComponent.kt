package com.sm.realtorhelper.di.components

import android.app.Application
import com.sm.realtorhelper.MainApplication
import com.sm.realtorhelper.di.modules.*
import dagger.BindsInstance
import dagger.Component
import dagger.android.support.AndroidSupportInjectionModule
import javax.inject.Singleton

@Singleton
@Component(
    modules = [
        AndroidSupportInjectionModule::class,
        AppModule::class,
        LocalSourceModule::class,
        MapperModule::class,
        RepositoryModule::class,
        UseCaseModule::class,
        ActivityModule::class,
        FragmentModule::class,
        ViewModelModule::class,
    ]
)
interface AppComponent {
    @Component.Factory
    interface Factory {
        fun create(@BindsInstance application: Application): AppComponent
    }

    fun inject(mainApplication: MainApplication)
}
