package com.sm.realtorhelper.di.utils.fragments

import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentFactory
import javax.inject.Inject
import javax.inject.Provider
import javax.inject.Singleton

@Singleton
class FragmentFactory @Inject constructor(
    private val fragments : Map<Class<out Fragment>, @JvmSuppressWildcards Provider<Fragment>>
) : FragmentFactory() {

    override fun instantiate(classLoader: ClassLoader, className: String): Fragment {
        val fragmentClass = loadFragmentClass(classLoader,className)
        return fragments[fragmentClass]?.get() ?: super.instantiate(classLoader, className)
    }
}
