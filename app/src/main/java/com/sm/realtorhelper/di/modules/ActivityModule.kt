package com.sm.realtorhelper.di.modules

import com.sm.realtorhelper.presentation.activities.MainActivity
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
interface ActivityModule {

    @ContributesAndroidInjector
    fun contributeMainActivity() : MainActivity
}
