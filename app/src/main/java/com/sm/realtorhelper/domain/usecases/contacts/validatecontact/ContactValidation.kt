package com.sm.realtorhelper.domain.usecases.contacts.validatecontact

import com.sm.realtorhelper.domain.models.Contact
import java.util.*

sealed class ContactValidation {

    data class Success(
        val contact: Contact
    ): ContactValidation()

    data class Failure(
        val errors: EnumMap<ContactField, ErrorType?>
    ): ContactValidation()
}

enum class ContactField {
    FIRST_NAME,
    SECOND_NAME,
    PHONE,
    ADDRESS,
    EMAIL,
}

enum class ErrorType {
    EMPTY,
    NOT_CORRECT,
}