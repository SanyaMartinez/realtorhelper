package com.sm.realtorhelper.domain.usecases.contacts.getcontactscount

import kotlinx.coroutines.flow.Flow

interface GetContactsCountUseCase {
    operator fun invoke() : Flow<Int>
}