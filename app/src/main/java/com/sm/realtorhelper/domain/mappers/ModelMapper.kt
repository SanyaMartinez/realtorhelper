package com.sm.realtorhelper.domain.mappers

interface ModelMapper<DataModel, DomainModel> {

    fun mapToDomain(dataModel: DataModel): DomainModel

    fun mapToData(domainModel: DomainModel): DataModel
}
