package com.sm.realtorhelper.domain.usecases.contacts.addcontact

import com.sm.realtorhelper.domain.models.Contact

interface AddContactUseCase {
    suspend operator fun invoke(contact: Contact)
}