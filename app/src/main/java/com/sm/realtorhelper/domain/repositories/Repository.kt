package com.sm.realtorhelper.domain.repositories

import kotlinx.coroutines.flow.Flow

interface Repository<T> {
    fun getById(id: Int): Flow<T?>
    fun getAll(): Flow<List<T>>
    fun getCount(): Flow<Int>

    suspend fun add(item: T)
}
