package com.sm.realtorhelper.domain.usecases.objects.getall

import com.sm.realtorhelper.domain.models.Object
import kotlinx.coroutines.flow.Flow

interface GetAllObjectsUseCase {
    operator fun invoke() : Flow<List<Object>>
}