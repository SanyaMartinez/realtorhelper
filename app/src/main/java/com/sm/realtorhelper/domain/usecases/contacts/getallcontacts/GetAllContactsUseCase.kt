package com.sm.realtorhelper.domain.usecases.contacts.getallcontacts

import com.sm.realtorhelper.domain.models.Contact
import kotlinx.coroutines.flow.Flow

interface GetAllContactsUseCase {
    operator fun invoke() : Flow<List<Contact>>
}