package com.sm.realtorhelper.domain.usecases.objects.getobjectscount

import com.sm.realtorhelper.domain.repositories.ObjectsRepository
import kotlinx.coroutines.flow.Flow
import javax.inject.Inject

class GetObjectsCountUseCaseImpl @Inject constructor(
    private val objectsRepository: ObjectsRepository
): GetObjectsCountUseCase {

    override fun invoke(): Flow<Int> = objectsRepository.getCount()
}