package com.sm.realtorhelper.domain.usecases.contacts.validatecontact

import com.sm.realtorhelper.data.models.ContactInputInfo
import com.sm.realtorhelper.domain.models.Contact
import kotlinx.coroutines.flow.MutableSharedFlow
import java.util.*
import javax.inject.Inject

class ValidateContactUseCaseImpl @Inject constructor(): ValidateContactUseCase {

    private val errors: EnumMap<ContactField, ErrorType?> = EnumMap(ContactField::class.java)

    override val validationResult: MutableSharedFlow<ContactValidation?> = MutableSharedFlow()

    override suspend fun validate(contactInputInfo: ContactInputInfo) {
        contactInputInfo.run {
            validateFirstName(firstName)
            validateSecondName(secondName)
            validatePhone(phone)
            validateAddress(address)
            validateEmail(email)

            val hasErrors = errors.values.any { it != null }
            val result = if (hasErrors) ContactValidation.Failure(errors)
            else {
                val contact = Contact(
                    firstName = firstName ?: "",
                    secondName = secondName ?: "",
                    phone = phone ?: "",
                    address = address,
                    email = email,
                    notes = null,
                    objects = null
                )
                ContactValidation.Success(contact)
            }

            validationResult.emit(result)
        }
    }

    private fun validateFirstName(firstName: String?) {
        errors[ContactField.FIRST_NAME] = if (firstName.isNullOrEmpty()) ErrorType.EMPTY
        else null
    }

    private fun validateSecondName(secondName: String?) {
        errors[ContactField.SECOND_NAME] = null
    }

    private fun validatePhone(phone: String?) {
        errors[ContactField.PHONE] = when {
            phone.isNullOrEmpty() -> ErrorType.EMPTY
            phone.length != ALLOWED_PHONE_LENGTH -> ErrorType.NOT_CORRECT
            else -> null
        }
    }

    private fun validateAddress(address: String?) {
        errors[ContactField.ADDRESS] = null
    }

    private fun validateEmail(email: String?) {
        errors[ContactField.EMAIL] = null
    }

    companion object {
        private const val ALLOWED_PHONE_LENGTH = 11
    }
}
