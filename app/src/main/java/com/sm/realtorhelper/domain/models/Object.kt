package com.sm.realtorhelper.domain.models

data class Object(
    val id: Int = 0,
    val phone: String,
    val address: String,
    val description: String,
    val price: String,
    val square: String,
    val latitude: Double,
    val longitude: Double,
)
