package com.sm.realtorhelper.domain.models

data class Contact(
    val id: Int = 0,
    val firstName: String,
    val secondName: String?,
    val phone: String,
    val address: String?,
    val email: String?,
    val notes: String?,
    val objects: String?,
)
