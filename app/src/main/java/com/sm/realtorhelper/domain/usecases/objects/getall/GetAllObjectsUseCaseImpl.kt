package com.sm.realtorhelper.domain.usecases.objects.getall

import com.sm.realtorhelper.domain.models.Object
import com.sm.realtorhelper.domain.repositories.ObjectsRepository
import kotlinx.coroutines.flow.Flow
import javax.inject.Inject

class GetAllObjectsUseCaseImpl @Inject constructor(
    private val objectsRepository: ObjectsRepository
): GetAllObjectsUseCase {

    override fun invoke(): Flow<List<Object>> = objectsRepository.getAll()
}