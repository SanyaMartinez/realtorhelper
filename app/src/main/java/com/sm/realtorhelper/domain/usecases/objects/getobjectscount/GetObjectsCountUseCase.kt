package com.sm.realtorhelper.domain.usecases.objects.getobjectscount

import kotlinx.coroutines.flow.Flow

interface GetObjectsCountUseCase {
    operator fun invoke() : Flow<Int>
}