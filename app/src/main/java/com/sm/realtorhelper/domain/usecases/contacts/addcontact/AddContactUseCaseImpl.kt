package com.sm.realtorhelper.domain.usecases.contacts.addcontact

import com.sm.realtorhelper.domain.models.Contact
import com.sm.realtorhelper.domain.repositories.ContactsRepository
import javax.inject.Inject

class AddContactUseCaseImpl @Inject constructor(
    private val contactsRepository: ContactsRepository
): AddContactUseCase {
    override suspend operator fun invoke(contact: Contact) = contactsRepository.add(contact)
}
