package com.sm.realtorhelper.domain.usecases.contacts.getcontactscount

import com.sm.realtorhelper.domain.repositories.ContactsRepository
import kotlinx.coroutines.flow.Flow
import javax.inject.Inject

class GetContactsCountUseCaseImpl @Inject constructor(
    private val contactsRepository: ContactsRepository
): GetContactsCountUseCase {

    override fun invoke(): Flow<Int> = contactsRepository.getCount()
}