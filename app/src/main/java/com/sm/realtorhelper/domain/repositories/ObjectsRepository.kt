package com.sm.realtorhelper.domain.repositories

import com.sm.realtorhelper.domain.models.Object

interface ObjectsRepository: Repository<Object>
