package com.sm.realtorhelper.domain.usecases.contacts.getcontact

import com.sm.realtorhelper.domain.models.Contact
import com.sm.realtorhelper.domain.repositories.ContactsRepository
import kotlinx.coroutines.flow.Flow
import javax.inject.Inject

class GetContactUseCaseImpl @Inject constructor(
    private val contactsRepository: ContactsRepository
): GetContactUseCase {

    override fun invoke(id: Int): Flow<Contact?> = contactsRepository.getById(id)
}
