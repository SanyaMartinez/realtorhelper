package com.sm.realtorhelper.domain.usecases.contacts.validatecontact

import com.sm.realtorhelper.data.models.ContactInputInfo
import kotlinx.coroutines.flow.SharedFlow

interface ValidateContactUseCase {
    val validationResult: SharedFlow<ContactValidation?>
    suspend fun validate(contactInputInfo: ContactInputInfo)
}
