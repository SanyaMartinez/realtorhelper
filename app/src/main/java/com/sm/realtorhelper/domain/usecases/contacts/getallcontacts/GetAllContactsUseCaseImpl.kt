package com.sm.realtorhelper.domain.usecases.contacts.getallcontacts

import com.sm.realtorhelper.domain.models.Contact
import com.sm.realtorhelper.domain.repositories.ContactsRepository
import kotlinx.coroutines.flow.Flow
import javax.inject.Inject

class GetAllContactsUseCaseImpl @Inject constructor(
    private val contactsRepository: ContactsRepository
): GetAllContactsUseCase {

    override fun invoke(): Flow<List<Contact>> = contactsRepository.getAll()
}