package com.sm.realtorhelper.domain.repositories

import com.sm.realtorhelper.domain.models.Contact

interface ContactsRepository: Repository<Contact>
