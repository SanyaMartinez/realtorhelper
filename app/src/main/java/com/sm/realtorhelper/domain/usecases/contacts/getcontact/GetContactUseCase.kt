package com.sm.realtorhelper.domain.usecases.contacts.getcontact

import com.sm.realtorhelper.domain.models.Contact
import kotlinx.coroutines.flow.Flow

interface GetContactUseCase {
    operator fun invoke(id: Int) : Flow<Contact?>
}