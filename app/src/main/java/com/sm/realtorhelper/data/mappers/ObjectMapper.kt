package com.sm.realtorhelper.data.mappers

import com.sm.realtorhelper.data.models.ObjectEntity
import com.sm.realtorhelper.domain.mappers.ModelMapper
import com.sm.realtorhelper.domain.models.Object

class ObjectMapper : ModelMapper<ObjectEntity, Object> {
    override fun mapToDomain(dataModel: ObjectEntity) = Object(
        id = dataModel.id,
        phone = dataModel.phone,
        address = dataModel.address,
        description = dataModel.description,
        price = dataModel.price,
        square = dataModel.square,
        latitude = dataModel.latitude,
        longitude = dataModel.longitude
    )

    override fun mapToData(domainModel: Object) = ObjectEntity(
        phone = domainModel.phone,
        address = domainModel.address,
        description = domainModel.description,
        price = domainModel.price,
        square = domainModel.square,
        latitude = domainModel.latitude,
        longitude = domainModel.longitude
    )
}
