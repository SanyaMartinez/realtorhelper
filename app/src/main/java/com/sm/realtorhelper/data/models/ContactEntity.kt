package com.sm.realtorhelper.data.models

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity
data class ContactEntity(
    val firstName: String,
    val secondName: String?,
    val phone: String,
    val address: String?,
    val email: String?,
    val notes: String?,
    val objects: String?,
) {
    @PrimaryKey(autoGenerate = true)
    var id: Int = 0
}
