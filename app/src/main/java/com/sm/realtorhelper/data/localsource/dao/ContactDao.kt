package com.sm.realtorhelper.data.localsource.dao

import androidx.room.*
import com.sm.realtorhelper.data.models.ContactEntity
import kotlinx.coroutines.flow.Flow

@Dao
interface ContactDao {
    @Query("SELECT * FROM ContactEntity WHERE id=:id")
    fun getById(id: Int): Flow<ContactEntity?>

    @Query("SELECT * FROM ContactEntity")
    fun getAll(): Flow<List<ContactEntity>>

    @Query("SELECT COUNT(*) FROM ContactEntity")
    fun getCount(): Flow<Int>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insert(contactEntity: ContactEntity)
}
