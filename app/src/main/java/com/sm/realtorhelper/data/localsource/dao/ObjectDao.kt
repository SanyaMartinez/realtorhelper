package com.sm.realtorhelper.data.localsource.dao

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.sm.realtorhelper.data.models.ObjectEntity
import kotlinx.coroutines.flow.Flow

@Dao
interface ObjectDao {
    @Query("SELECT * FROM ObjectEntity WHERE id=:id")
    fun getById(id: Int): Flow<ObjectEntity?>

    @Query("SELECT * FROM ObjectEntity")
    fun getAll(): Flow<List<ObjectEntity>>

    @Query("SELECT COUNT(*) FROM ObjectEntity")
    fun getCount(): Flow<Int>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insert(objectEntity: ObjectEntity)
}
