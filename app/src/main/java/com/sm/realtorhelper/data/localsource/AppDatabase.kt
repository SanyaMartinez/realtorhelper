package com.sm.realtorhelper.data.localsource

import androidx.room.Database
import androidx.room.RoomDatabase
import com.sm.realtorhelper.data.models.ContactEntity
import com.sm.realtorhelper.data.localsource.AppDatabase.Companion.CURRENT_DB_VERSION
import com.sm.realtorhelper.data.localsource.dao.ContactDao
import com.sm.realtorhelper.data.localsource.dao.ObjectDao
import com.sm.realtorhelper.data.models.ObjectEntity

@Database(
    entities = [
        ContactEntity::class,
        ObjectEntity::class,
    ],
    version = CURRENT_DB_VERSION
)
abstract class AppDatabase : RoomDatabase() {

    abstract fun contactDao(): ContactDao

    abstract fun objectDao(): ObjectDao

    companion object {
        private object DatabaseVersion {
            const val V1 = 1
        }

        const val DB_NAME = "RealtorHelperDB"
        const val CURRENT_DB_VERSION = DatabaseVersion.V1
    }
}
