package com.sm.realtorhelper.data.models

data class ContactInputInfo(
    val firstName: String?,
    val secondName: String?,
    val phone: String?,
    val address: String?,
    val email: String?,
)
