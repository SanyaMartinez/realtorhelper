package com.sm.realtorhelper.data.repositories

import com.sm.realtorhelper.data.localsource.dao.ObjectDao
import com.sm.realtorhelper.data.mappers.ObjectMapper
import com.sm.realtorhelper.domain.models.Object
import com.sm.realtorhelper.domain.repositories.ObjectsRepository
import kotlinx.coroutines.flow.transform
import javax.inject.Inject

class ObjectsRepositoryImpl @Inject constructor(
    private val objectDao: ObjectDao,
    private val objectMapper: ObjectMapper,
): ObjectsRepository {

    override fun getById(id: Int) = objectDao
        .getById(id)
        .transform { dataModel ->
            val domainModel = if (dataModel == null) null
            else objectMapper.mapToDomain(dataModel)
            emit(domainModel)
        }

    override fun getAll() = objectDao
        .getAll()
        .transform {
            val domainModels = it.map { dataModel ->
                objectMapper.mapToDomain(dataModel)
            }

            //TODO
            val hardcodeData = listOf(
                Object(
                    id = 1,
                    phone = "88005553535",
                    address = "г. Челябинск, Братьев Кашириных, д.119",
                    description = "2-комнатная квартира",
                    price = "1000000",
                    square = "50",
                    latitude = 55.186725,
                    longitude = 61.331888
                ),
                Object(
                    id = 2,
                    phone = "89008001234",
                    address = "г. Челябинск, Ленина, д.19",
                    description = "3-комнатная квартира",
                    price = "2020000",
                    square = "76",
                    latitude = 55.186112,
                    longitude = 61.334443
                ),
                Object(
                    id = 3,
                    phone = "89994440000",
                    address = "г. Челябинск, Гагарина, д.1",
                    description = "1-комнатная квартира",
                    price = "800000",
                    square = "31",
                    latitude = 55.183686,
                    longitude = 61.330235
                ),
            )

//            emit(domainModels)
            emit(hardcodeData)
        }

    override fun getCount() = objectDao.getCount()

    override suspend fun add(item: Object) {
        val dataModel = objectMapper.mapToData(item)
        objectDao.insert(dataModel)
    }
}
