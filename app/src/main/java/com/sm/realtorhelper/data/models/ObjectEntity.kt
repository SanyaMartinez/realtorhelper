package com.sm.realtorhelper.data.models

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity
data class ObjectEntity(
    val phone: String,
    val address: String,
    val description: String,
    val price: String,
    val square: String,
    val latitude: Double,
    val longitude: Double,
) {
    @PrimaryKey(autoGenerate = true)
    var id: Int = 0
}
