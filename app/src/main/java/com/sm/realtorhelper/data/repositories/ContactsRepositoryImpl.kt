package com.sm.realtorhelper.data.repositories

import com.sm.realtorhelper.data.localsource.dao.ContactDao
import com.sm.realtorhelper.data.mappers.ContactMapper
import com.sm.realtorhelper.domain.models.Contact
import com.sm.realtorhelper.domain.repositories.ContactsRepository
import kotlinx.coroutines.flow.transform
import javax.inject.Inject

class ContactsRepositoryImpl @Inject constructor(
    private val contactDao: ContactDao,
    private val contactMapper: ContactMapper,
): ContactsRepository {

    override fun getById(id: Int) = contactDao
        .getById(id)
        .transform { dataModel ->
            val domainModel = if (dataModel == null) null
            else contactMapper.mapToDomain(dataModel)
            emit(domainModel)
        }

    override fun getAll() = contactDao
        .getAll()
        .transform {
            val domainModels = it.map { dataModel ->
                contactMapper.mapToDomain(dataModel)
            }
            emit(domainModels)
        }

    override fun getCount() = contactDao.getCount()

    override suspend fun add(contact: Contact) {
        val dataModel = contactMapper.mapToData(contact)
        contactDao.insert(dataModel)
    }
}
