package com.sm.realtorhelper.data.mappers

import com.sm.realtorhelper.data.models.ContactEntity
import com.sm.realtorhelper.domain.mappers.ModelMapper
import com.sm.realtorhelper.domain.models.Contact

class ContactMapper: ModelMapper<ContactEntity, Contact> {
    override fun mapToDomain(dataModel: ContactEntity) = Contact(
        id = dataModel.id,
        firstName = dataModel.firstName,
        secondName = dataModel.secondName,
        phone = dataModel.phone,
        address = dataModel.address,
        email = dataModel.email,
        notes = dataModel.notes,
        objects = dataModel.objects
    )

    override fun mapToData(domainModel: Contact) = ContactEntity(
        firstName = domainModel.firstName,
        secondName = domainModel.secondName,
        phone = domainModel.phone,
        address = domainModel.address,
        email = domainModel.email,
        notes = domainModel.notes,
        objects = domainModel.objects
    )
}
